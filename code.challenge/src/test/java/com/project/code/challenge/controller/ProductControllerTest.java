package com.project.code.challenge.controller;

import com.project.code.challenge.entity.Product;
import com.project.code.challenge.service.ProductService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ProductController.class)

public class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductService service;


    @Test
    void getProductTest() throws Exception {
        Product product = new Product();
        product.setId(1001);
        product.setName("TestProduct");
        when(service.getProductById(1001)).thenReturn(product);
        RequestBuilder request = MockMvcRequestBuilders.get("/product/1001");
        MvcResult result = mvc.perform(request).andReturn();
        String productAsString = "{\"id\":1001,\"quantity\":0,\"name\":\"TestProduct\",\"category\":null,\"description\":null,\"createdDate\":null,\"lastModifiedDate\":null}";
        assertEquals(productAsString, result.getResponse().getContentAsString());
    }
}
