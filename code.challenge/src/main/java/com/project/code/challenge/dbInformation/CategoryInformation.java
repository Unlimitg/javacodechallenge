package com.project.code.challenge.dbInformation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryInformation {

    private String category;
    private int productsAvailable;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getProductsAvailable() {
        return productsAvailable;
    }

    public void setProductsAvailable(int productsAvailable) {
        this.productsAvailable = productsAvailable;
    }
}
