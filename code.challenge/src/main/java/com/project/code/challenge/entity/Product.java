package com.project.code.challenge.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "PRODUCT_TABLE")
public class Product {

    @Id
    private int id;
    private int quantity;
    private String name;
    private String category;
    private String description;
    private Date createdDate;
    private Date lastModifiedDate;

}
