package com.project.code.challenge.dbData;

import com.project.code.challenge.entity.Product;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;


public class dbColumnsData {


    public List<Product> productList(){

        Product product = new Product();
        product.setId(1001);
        product.setName("Dell 5401");
        product.setCategory("Laptop");
        product.setDescription("Dell description");
        product.setQuantity(12);
        product.setCreatedDate(Date.valueOf("2020-05-20"));
        product.setLastModifiedDate(Date.valueOf("2020-06-30"));

        Product product1 = new Product();
        product1.setId(1002);
        product1.setName("Dell U2413");
        product1.setCategory("Monitor");
        product1.setDescription("Dell monitor");
        product1.setQuantity(15);
        product1.setCreatedDate(Date.valueOf("2020-05-20"));
        product1.setLastModifiedDate(Date.valueOf("2020-06-30"));


        Product product2 = new Product();
        product2.setId(1003);
        product2.setName("Samsung");
        product2.setCategory("Monitor");
        product2.setDescription("Samsung monitor");
        product2.setQuantity(9);
        product2.setCreatedDate(Date.valueOf("2020-06-30"));
        product2.setLastModifiedDate(Date.valueOf("2020-07-15"));

        List<Product> productList = new ArrayList();
        productList.add(product);
        productList.add(product1);
        productList.add(product2);

        return productList;

    }



}
