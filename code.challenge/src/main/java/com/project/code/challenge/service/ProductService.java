package com.project.code.challenge.service;

import com.project.code.challenge.entity.Product;
import com.project.code.challenge.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository repository;

    public Product saveProduct(Product product){
        return repository.save(product);
    }

    public List<Product> saveProducts(List<Product> products){
        return repository.saveAll(products);
    }

    public List<Product> getProducts(){
        return repository.findAll();
    }

    public Page<Product> getProductsBy(Pageable pageable){

        Page<Product> page = repository.findAll(PageRequest.of(pageable.getPageNumber()-1, pageable.getPageSize(), Sort.by(Sort.Direction.DESC, "name")));

        return  page;
    }

    public Product getProductById(int id){
        return repository.findById(id).orElse(null);
    }

    public Product getProductByName(String name){
        return repository.findByName(name);
    }

    public String deleteProduct(int id){
        repository.deleteById(id);
        return "product removed "+id;
    }

    public Product orderProduct(int id, int quantity) throws Exception {
        Product existingProduct = repository.findById(id).orElse(null);
        if(existingProduct.getQuantity() < quantity){
            throw new Exception("The requested quantity in the order is greater than the existing quantity");
        }else if (existingProduct.getQuantity() - quantity <= 0){
            throw new Exception("The requested quantity in the order is greater than the existing quantity, please reduce the amount");
        }
        return repository.save(existingProduct);
    }
}
