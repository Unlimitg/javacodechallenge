package com.project.code.challenge.controller;

import com.project.code.challenge.dbData.dbColumnsData;
import com.project.code.challenge.dbInformation.CategoryInformation;
import com.project.code.challenge.entity.Product;
import com.project.code.challenge.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RestController
@EnableWebMvc
@EnableSpringDataWebSupport
public class ProductController {

    @Autowired
    private ProductService service;

    @PostMapping("/products/{id}/order/{quantity}")
    public void orderProducts(@PathVariable int id,@PathVariable int quantity){

        try {
            service.orderProduct(id,quantity);
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @PostMapping("/db")
    public List<Product>  db(){
        dbColumnsData db = new dbColumnsData();
      return  service.saveProducts(db.productList());
    }

    @PostMapping("/addProduct")
    public Product addProduct(@RequestBody Product product){
        return service.saveProduct(product);
    }

    @PostMapping("/addProducts")
    public List<Product> addProduct(@RequestBody List<Product> products){
        return service.saveProducts(products);
    }

    @GetMapping("/allProducts")
    public List<Product> findAllProducts(){
        return service.getProducts();
    }

//    @RequestMapping("/products")
//    public void getProductsBy(@RequestParam  String orderBy, @RequestParam  String direction, @RequestParam int page, @RequestParam int pageSize){
//    }

    @RequestMapping("/products")
    public Page<Product> getProductsBy(@RequestParam  String orderBy, @RequestParam  String direction, Pageable pageable){

        return service.getProductsBy(pageable);
    }

    @RequestMapping(value="/categories")
    public Page<List<CategoryInformation>> categoriesInfo() throws Exception{
        CategoryInformation category0 = new CategoryInformation();
        category0.setCategory("Laptop");
        CategoryInformation category1 = new CategoryInformation();
        category1.setCategory("Monitor");
        int category0Counter = 0;
        int category1Counter = 0;
        List<Product> productList = service.getProducts();
        Iterator iterator = productList.iterator();
        while(iterator.hasNext()){
            Product prod = (Product) iterator.next();
            if(prod.getCategory().equals("Laptop")){
                category0Counter++;
            }else{
                category1Counter++;
            }
        }
        category0.setProductsAvailable(category0Counter);
        category1.setProductsAvailable(category1Counter);
        List<CategoryInformation> dbInfo = new ArrayList();
        dbInfo.add(category0);
        dbInfo.add(category1);
        final Page<List<CategoryInformation>> page = new PageImpl(dbInfo);
        return page;
    }

    @GetMapping("/product/{id}")
    public Product findProductById(@PathVariable int id){
        return service.getProductById(id);
    }

    @PutMapping("/update")
    public void updateProduct(@RequestBody Product product){

    }

    @DeleteMapping("/delete/{id}")
    public String deleteProduct(@PathVariable int id){
        return service.deleteProduct(id);
    }
}
